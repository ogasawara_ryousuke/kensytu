from flickrapi import FlickrAPI
from urllib.request import urlretrieve
import os, time, sys
# import ssl

key = "bb239baddf219429ce8969bd8e55ef91"
secret = "801fe03cf18fe64b"

wait_time = 1
# 写真をダウンロードする間隔
keyword = sys.argv[1]
# 検索ワードの引数
imgdir = os.path.join(os.getcwd(), "images")
# ディレクトリを作りつつパスをつなげている
savedir = os.path.join(imgdir, keyword)
# キーワードの名前をつけてくれてる

flickr = FlickrAPI(key, secret, format="parsed-json")

result = flickr.photos.search(
    text = keyword,
    per_page = 50,
    media = "photos",
    sort = "relevance",
    safe_search = 1,
    extras = "url_q, license"
)
#取得したデータを検索　動画データもあるので写真でrelevance:関連度でソートして10枚ずつ
# print(result)
# print(result.keys())


if not os.path.exists(savedir):
    os.makedirs(savedir)


for dl_photo in (result["photos"]["photo"]):
    url_q = dl_photo["url_q"]
    filepath = os.path.join(savedir, dl_photo["id"] + ".jpg")

    if os.path.exists(filepath):
        continue
    # すでにダウンロードしてたらcontinue
    urlretrieve(url_q, filepath)
    # 保存
    time.sleep(wait_time)
    # 待つ